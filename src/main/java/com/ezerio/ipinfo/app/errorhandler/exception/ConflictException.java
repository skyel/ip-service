package com.ezerio.ipinfo.app.errorhandler.exception;

public class ConflictException extends RuntimeException {

    public ConflictException(String detail) {
        super(detail);
    }

}
