package com.ezerio.ipinfo.dao;

import com.ezerio.ipinfo.entity.ForeignExchange;
import com.ezerio.ipinfo.entity.Stat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ForeignExchangeDao extends JpaRepository<ForeignExchange, Long>  {

    ForeignExchange getByCurrency(String currency);

}
