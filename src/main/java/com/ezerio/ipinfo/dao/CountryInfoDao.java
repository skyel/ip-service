package com.ezerio.ipinfo.dao;

import com.ezerio.ipinfo.entity.CountryInfo;
import com.ezerio.ipinfo.entity.Currency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryInfoDao extends JpaRepository<CountryInfo, Long> {

    CountryInfo findByIsoCode(String isoCode);
}
