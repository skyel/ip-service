package com.ezerio.ipinfo.api.rest;

import com.ezerio.ipinfo.api.dto.CountryInfoApiDto;

public interface CountryInfoRest {

    public CountryInfoApiDto getInfoCountry(String countryCode);
}
