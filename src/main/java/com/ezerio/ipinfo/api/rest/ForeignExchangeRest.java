package com.ezerio.ipinfo.api.rest;

import com.ezerio.ipinfo.api.dto.ForexApiDto;

public interface ForeignExchangeRest {

    public ForexApiDto getForeignExchange(String currency);

}
