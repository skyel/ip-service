package com.ezerio.ipinfo.api.rest;

import com.ezerio.ipinfo.api.dto.IpInfoDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class IpCountryRestImpl implements IpCountryRest {

    @Autowired
    @Qualifier("api.ip.country")
    private RestTemplate restTemplate;

    @Override
    public String getCountryCode(String ip) {
        final String url = "/ip?" + ip;
        ResponseEntity<IpInfoDto> response = restTemplate.getForEntity(url, IpInfoDto.class);
        return response.getBody().getCountryCode();
    }

}
