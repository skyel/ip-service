package com.ezerio.ipinfo.api.rest;

public interface IpCountryRest {

    public String getCountryCode(String ip);

}
