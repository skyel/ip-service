package com.ezerio.ipinfo.service;

import com.ezerio.ipinfo.api.dto.ForexApiDto;
import com.ezerio.ipinfo.api.dto.ForexDto;
import com.ezerio.ipinfo.api.rest.ForeignExchangeRest;
import com.ezerio.ipinfo.dao.ForeignExchangeDao;
import com.ezerio.ipinfo.entity.ForeignExchange;
import com.ezerio.ipinfo.utils.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ForeignExchangeServiceImpl implements ForeignExchangeService {

    @Value("${ipinfo.forex.key}")
    private String key;

    @Value("${ipinfo.forex.minutes.update}")
    private int minutesToUpdate;

    @Autowired
    private ForeignExchangeRest foreignExchangeRest;

    @Autowired
    private ForeignExchangeDao foreignExchangeDao;

    @Override
    public Float getForeignExchange(String currency) {
        ForeignExchange foreignExchange = foreignExchangeDao.getByCurrency(currency);
        if(foreignExchange == null || isExpire(foreignExchange.getTimestamp())) {
            ForexDto forexDto = getForeignExchangeOnline(currency);
            updateRepoForeignExchange(forexDto, foreignExchange);
            return forexDto.getRate();
        }
       return foreignExchange.getRate();
    }

    private boolean isExpire(long date) {
        if(TimeUtil.minutesGone(date) >= minutesToUpdate) {
            return true;
        }
        return false;
    }

    private ForexDto getForeignExchangeOnline(String currency) {
        ForexApiDto forexApiDto = foreignExchangeRest.getForeignExchange(currency);
        return buildForexDto(forexApiDto, currency);
    }

    private void updateRepoForeignExchange(ForexDto dto, ForeignExchange entity) {
        if(entity == null) {
            entity = new ForeignExchange();
            entity.setCurrency(dto.getCurrency());
        }
        entity.setTimestamp(dto.getTimestamp());
        entity.setRate(dto.getRate());
        foreignExchangeDao.save(entity);
    }

    private ForexDto buildForexDto(ForexApiDto result, String currency) {
        return new ForexDto(currency, result.getRates().get(currency), Long.parseLong(result.getTimestamp()));
    }

}