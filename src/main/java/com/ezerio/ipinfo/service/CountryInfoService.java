package com.ezerio.ipinfo.service;

import com.ezerio.ipinfo.api.dto.CountryInfoDto;

public interface CountryInfoService {

    public CountryInfoDto getCountryInfo(String countryCode);

}
