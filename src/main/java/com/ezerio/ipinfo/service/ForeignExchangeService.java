package com.ezerio.ipinfo.service;

public interface ForeignExchangeService {

    public Float getForeignExchange(String symbol);

}
