package com.ezerio.ipinfo.service;

import com.ezerio.ipinfo.dto.DistanceDto;

public interface StatService {

    public DistanceDto getStats();

}
