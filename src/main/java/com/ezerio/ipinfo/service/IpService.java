package com.ezerio.ipinfo.service;

import com.ezerio.ipinfo.dto.DistanceDto;
import com.ezerio.ipinfo.dto.TraceDto;

public interface IpService {

    public TraceDto getIpInformation(String ip);

}
