package com.ezerio.ipinfo.service;

import com.ezerio.ipinfo.api.rest.IpCountryRest;
import com.ezerio.ipinfo.api.dto.CountryInfoDto;
import com.ezerio.ipinfo.api.dto.CurrencyDto;
import com.ezerio.ipinfo.dao.StatDao;
import com.ezerio.ipinfo.dto.TraceDto;
import com.ezerio.ipinfo.entity.Stat;
import com.ezerio.ipinfo.utils.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class IpServiceImpl implements IpService {

    @Autowired
    private IpCountryRest ipCountryRest;

    @Autowired
    private CountryInfoService countryInfoService;

    @Autowired
    private ForeignExchangeService foreignExchangeService;

    @Autowired
    private StatDao statDao;

    @Override
    public TraceDto getIpInformation(String ip) {
        String countryCode = ipCountryRest.getCountryCode(ip);
        CountryInfoDto countryInfoDto = countryInfoService.getCountryInfo(countryCode);
        TraceDto traceDto = build(countryInfoDto);
        traceDto.setIp(ip);
        traceDto.setDate(TimeUtil.now());
        traceDto.setCurrency(calculateCurrency(countryInfoDto.getCurrency()));
        traceDto.setTimes(calculateTime(countryInfoDto.getTimes()));
        statDao.save(new Stat(countryInfoDto.getIsoCode()));
        return traceDto;
    }

    private TraceDto build(CountryInfoDto dto) {
        return TraceDto.builder()
                .country(dto.getCountry())
                .isoCode(dto.getIsoCode())
                .language(dto.getLanguages())
                .estimateDistance(dto.getEstimateDistance() + " kms")
                .build();
    }

    private List<String> calculateTime(List<String> times) {
        return times.stream()
                .map(TimeUtil::getCurrentTime)
                .collect(Collectors.toList());
    }

    private List<String> calculateCurrency(List<CurrencyDto> currencies) {
        List<String> result = new ArrayList<>();
        for(CurrencyDto currency : currencies) {
            try {
                final String curr = fetchCurrency(currency.getCode(), currency.getSymbol());
                result.add(curr);
            } catch (Exception e) {
                continue;
            }
        }
        return result;
    }

    private String fetchCurrency(String code, String symbol) {
        final Float rate = foreignExchangeService.getForeignExchange(code);
        return String.format("%s (1 EUR = %s %s)", code, rate, symbol);
    }

}