package com.ezerio.ipinfo.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class IpDto {

    private static final String REGEXP_IP = "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

    @Pattern(regexp = REGEXP_IP, message = "Invalid ip address")
    private String ip;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
