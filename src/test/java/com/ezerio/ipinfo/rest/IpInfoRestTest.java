package com.ezerio.ipinfo.rest;

import com.ezerio.ipinfo.dto.IpDto;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.reactive.server.WebTestClient;

@TestApiConfig
public class IpInfoRestTest {

    @Autowired
    private WebTestClient webTestClient;

    @Test
    public void testOkRequest() {
        IpDto validIp = new IpDto();
        validIp.setIp("1.1.1.1");
        webTestClient.post()
                .uri(IpInfoRest.BASE_PATH + IpInfoRest.TRACE)
                .bodyValue(validIp)
                .exchange()
                .expectStatus().isOk();
    }

    @Test
    public void testBadRequest() {
        IpDto invalidIp = new IpDto();
        invalidIp.setIp("999.999.999.999");
        webTestClient.post()
                .uri(IpInfoRest.BASE_PATH + IpInfoRest.TRACE)
                .bodyValue(invalidIp)
                .exchange()
                .expectStatus().isBadRequest();
    }
}
